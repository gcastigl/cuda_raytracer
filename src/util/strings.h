#ifndef UTIL_STRINGS_H
#define UTIL_STRINGS_H

#include "../math/math3d.h"

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

inline bool ends_with(string const & value, string const & ending) {
  if (ending.size() > value.size()) 
    return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline bool readNFloats(string buffer, float *values, int n) {
  int i = 0;
  stringstream ss;
  ss << buffer;
  while (i < n && ss.rdbuf()->in_avail()) {
    float f;
    if (ss >> f) {
      values[i] = f;
      i++;
    }
  }
  if (i != n) {
    cout << "[ERROR] Could not read enough floats from buffer\n";
  }
  return i == n;
}

inline bool readNInts(string buffer, int *values, int n) {
  int i = 0;
  stringstream ss;
  ss << buffer;
  while (i < n && ss.rdbuf()->in_avail()) {
    int f;
    if (ss >> f) {
      values[i] = f;
      i++;
    }
  }
  if (i != n) {
    cout << "[ERROR] Could not read enough ints from buffer\n";
  }
  return i == n;
}

inline bool readVector3f(string buffer, t_vec* vec) {
  stringstream ss;
  ss << buffer;
  int i = 0;
  while (i < 3 && ss.rdbuf()->in_avail()) {
    float f;
    if (ss >> f) {
      if (i == 0) {
        vec->x = f;
      } else if (i == 1) {
        vec->y = f;
      } else {
        vec->z = f;
      }
      i++;
    }
  }
  if (i != 3) {
    cout << "[ERROR] Could not read enough floats from buffer\n";
  }
  return i == 3;
}

#endif

