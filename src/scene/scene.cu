#include "types.h"

__device__
void scene_copyCollisionValues(t_collision *destination, t_collision *source) {
  destination->geometry = source->geometry;
  destination->distance = source->distance;
  destination->index = source->index;
  vec_set(&destination->localPoint, &source->localPoint);
  vec_set(&destination->worldPoint, &source->worldPoint);
  vec_set(&destination->normal, &source->normal);
  vec_set(&destination->color, &source->color);
}

