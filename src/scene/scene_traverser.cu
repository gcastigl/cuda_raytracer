#include "scene_traverser.h"

#include "../math/transform.h"
#include "../math/sphere.h"
#include "../math/plane.h"
#include "../math/aabb.h"
#include "../mesh/triangle_mesh.h"

__device__ bool traverser_collision_distance(
  t_geometry *geometry, t_line *line, t_collision *collision
);

__device__ bool scene_traverser_closest_private(
  t_scene *scene, t_line *line, t_collision *collision, t_geometry *geomToIgnore
);

__device__ void localToWorldTransform(
  t_collision *collision, t_line *line, float scale
);

__device__ bool traverser_bounds_collision(t_bounds *bounds, t_line *line);


__device__
bool scene_traverser_closest(t_scene *scene, t_line *line, t_collision *collision) {
  return scene_traverser_closest_private(scene, line, collision, NULL);
}

__device__
bool scene_traverser_isCulled(t_scene *scene, t_line *line, float maxDistanceSq, t_geometry* geomToIgnore) {
  t_collision collision;
  bool hasCollision = scene_traverser_closest_private(scene, line, &collision, geomToIgnore);
  if (hasCollision && vec_distanceSq(&collision.worldPoint, &line->p0) > maxDistanceSq) {
    return false;
  }
  return hasCollision;
}

__device__ bool scene_traverser_closest_private(t_scene *scene, t_line *line, t_collision *collision, t_geometry *geomToIgnore) {
  t_collision closestCollision = {0};
  closestCollision.geometry = NULL;
  bool hasClosestCollision = false;
  t_geometry *geometries = scene->geometries;
  t_collision icollision;
  for (int i = 0; i < scene->geometriesCount; i++) {
    t_geometry *geometry = geometries + i;
    if (geomToIgnore != NULL && geomToIgnore == geometry) {
      continue;
    }
    bool hasCollision = traverser_collision_distance(geometry, line, &icollision);
    if (hasCollision && (!hasClosestCollision || icollision.distance < closestCollision.distance)) {
      scene_copyCollisionValues(&closestCollision, &icollision);
      hasClosestCollision = true;
    }
  }
  scene_copyCollisionValues(collision, &closestCollision);
  return hasClosestCollision;
}

__device__
bool traverser_collision_distance(t_geometry *geometry, t_line *line, t_collision *collision) {
  // printf("[DEBUG] Test collision with geometry: %s\n", geometry->name);
  t_line localLine;
  float scaleFactor;
  line_transform_inverse(line, &geometry->transform, &localLine, &scaleFactor);
  bool hasBoundsCollision = traverser_bounds_collision(&geometry->bounds, &localLine);
  if (!hasBoundsCollision) {
    return false;
  }
  bool hasCollision = false;
  if (geometry->type == SPHERE) {
    hasCollision = sphere_intersection_line_dist(&geometry->sphere, &localLine, collision);
  } else if (geometry->type == TRIANGLE_MESH) {
    hasCollision = triangle_mesh_intersection(&geometry->mesh, &localLine, collision);
  } else if (geometry->type == PLANE) {
    hasCollision = plane_intersection_line(&geometry->plane, &localLine, collision);
  }
  if (hasCollision) {
    collision->geometry = geometry;
    localToWorldTransform(collision, line, scaleFactor);
  }
  return hasCollision;
}

__device__ void localToWorldTransform(t_collision *collision, t_line *line, float scale) {
  transform_direction(&collision->geometry->transform, &collision->normal, &collision->normal);
  transform_point(&collision->geometry->transform, &collision->localPoint, &collision->worldPoint);
  collision->distance /= scale;
  // Fix normal if pointing in opposite direction
  if (vec_dot(&line->d, &collision->normal) > 0) {
    vec_scale(&collision->normal, -1);
  }
}

__device__ bool traverser_bounds_collision(t_bounds *bounds, t_line *line) {
  if (bounds->type == NONE) {
    return true;
  }
  if (bounds->type == AABB) {
    return aabb_intersects(&bounds->aabb, line);
  }
  printf("[ERROR] Unknown bounding type: %d. Collision is false.\n", bounds->type);
  return false;
}

