#ifndef SCENE_TRAVERSER_H
#define SCENE_TRAVERSER_H

#include "../scene/types.h"
#include "../mesh/types.h"

__device__
bool scene_traverser_closest(t_scene *scene, t_line *line, t_collision *collision);

__device__
bool scene_traverser_isCulled(t_scene *scene, t_line *line, float maxDistanceSq, t_geometry* geomToIgnore);

#endif
