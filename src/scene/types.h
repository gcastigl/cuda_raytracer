#ifndef SCENE_TYPES_H
#define SCENE_TYPES_H

#include "../math/math3d.h"
#include "../light/types.h"
#include "../mesh/types.h"

typedef enum  {MODE_COLOR, MODE_HIT, MODE_DISTANCE, MODE_NORMALS} t_render_mode;

typedef struct {
  t_vec origin;
  t_vec forward;
  t_vec up;
  t_vec right;
} t_camera;

typedef struct {
  float dz;
  float dw;
  float dh;
} t_frustum;

typedef struct {
  t_geometry *geometries;
  int geometriesCount;
  t_light *lights;
  int lightsCount;
} t_scene;

typedef struct {
  t_scene scene;
  t_camera camera;
  t_frustum frustum;
} t_world;

typedef struct {
  int startIndex;
  int endIndex;
  int width;
  int height;
} t_frame;

typedef struct {
  t_geometry *geometry;
  float distance;
  t_vec localPoint;
  t_vec worldPoint;
  t_vec normal;
  int index;
  t_vec color;
} t_collision;

typedef struct {
  int threads;
  t_render_mode renderMode;
} t_options;

__device__ void scene_copyCollisionValues(t_collision *destination, t_collision *source);

#endif

