#include <stdio.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>

#include "parser/parser.h"
#include "raytracer/raytracer.h"
#include "image/makepng.h"

__host__ void startApplication(char *sceneFileName);

__global__ void cuda_buildFrame(
  t_world *world, t_frame frame, t_collision *collisions, t_options options
);

__device__ void camera_buildRay(t_line *line, t_world *world, t_frame frame, int globalIndex);


int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("One argument required: path to scene file\n");
    return 0;
  }
  struct timeval tval_before, tval_after, tval_result;
  gettimeofday(&tval_before, NULL);
  startApplication(argv[1]);
  gettimeofday(&tval_after, NULL);
  timersub(&tval_after, &tval_before, &tval_result);
  printf("[INFO] Elapsed time: %ld.%06ld [s]\n", 
    (long int) tval_result.tv_sec, (long int) tval_result.tv_usec
  );
  return 0;
}

__host__ void startApplication(char *sceneFileName) {
  // Create & load raytracer information
  t_world world;
  t_frame frame;
  t_dmemory device;
  t_options options;
  bool loadedOK = parser_load(sceneFileName, &world, &frame, &device, &options);
  if (!loadedOK) {
    return;
  }
  int collisionSize = options.threads * sizeof(t_collision);
  t_collision *collisions = (t_collision*) malloc(collisionSize);
  pixel_t *imageBuffer = (pixel_t*) malloc(
    frame.width * frame.height * sizeof(pixel_t)
  );
  // Allocate device space for variables
  cudaMalloc((void **) &device.world, sizeof(t_world));
  cudaMalloc((void **) &device.collisions, collisionSize);

  // Copy information to device
  {
    t_world d_world;
    memcpy(&d_world, &world, sizeof(t_world));
    d_world.scene.geometries = device.geometries;
    d_world.scene.lights = device.lights;
    cudaMemcpy(
      device.world, &d_world, sizeof(t_world), cudaMemcpyHostToDevice
    );
  }

  // Execute kernel
  int kernelExecutions = frame.endIndex / options.threads;
  printf("[INFO] Executing kernel %d times\n", kernelExecutions);
  for (int i = 0; i < kernelExecutions; i++) {
    // printf("[DEBUG] Executing line: %d\n", i);
    frame.startIndex = i * options.threads;
    cuda_buildFrame<<<options.threads, 1>>>(device.world, frame, device.collisions, options);
    // Get (partial) results from device
    cudaMemcpy(
      collisions, device.collisions, collisionSize, cudaMemcpyDeviceToHost
    );
    for (int j = 0; j < options.threads; j++) {
      t_collision *collision = collisions + j;
      if (collision->geometry != NULL) {
          pixel_t *pixel = imageBuffer + (frame.startIndex + j);
          float distance = collision->distance;
          t_vec normal = collision->normal;
          t_vec color = collision->color;
          switch (options.renderMode) {
            case MODE_COLOR:
              pixel->r = MIN(1, MAX(color.x, 0));
              pixel->g = MIN(1, MAX(color.y, 0));
              pixel->b = MIN(1, MAX(color.z, 0));
              break;
            case MODE_HIT:
              pixel->r = 0.5;
              pixel->g = 0.5;
              pixel->b = 0.5;
              break;
            case MODE_DISTANCE:
              pixel->r = MIN(1 / distance, 1);
              pixel->g = MIN(1 / distance, 1);
              pixel->b = MIN(1 / distance, 1);
              break;
            case MODE_NORMALS:
              pixel->r = (normal.x + 1) / 2.0;
              pixel->g = (normal.y + 1) / 2.0;
              pixel->b = (normal.z + 1) / 2.0;
              break;
          } 
      }
    }
  }
  printf("[INFO] Building image\n");
  // Free device's memory
  cudaFree(device.world);
  cudaFree(device.collisions);
  cudaFree(device.lights);

  // Show result
  const char *filename = "test.png";
  const char *title = "Demo";
  writeImage(filename, frame.width, frame.height, imageBuffer, title);

  // Free host memory
  free(imageBuffer);
  free(collisions);
  free(world.scene.geometries);
  free(world.scene.lights);
}

__global__ 
void cuda_buildFrame(t_world *world, t_frame frame, t_collision *collisions, t_options options) {
  int localIndex = threadIdx.x + blockIdx.x * blockDim.x;
  int globalIndex = frame.startIndex + localIndex;
  if (frame.endIndex < globalIndex) {
    printf("Skipping index: %d\n", globalIndex);
    return;
  }
  t_line line;
  camera_buildRay(&line, world, frame, globalIndex);
  t_ray_info info;
  info.depth = 0;
  info.thread_local_index = localIndex;
  info.thread_global_index = globalIndex;
  info.renderMode = options.renderMode;
  raytracer_trace(&line, &world->scene, collisions + localIndex, &info);
}

__device__ void camera_buildRay(t_line *line, t_world *world, t_frame frame, int globalIndex) {
  int ix = globalIndex % frame.width;
  int iy = globalIndex / frame.width;
  // Camera
  t_camera *camera = &world->camera;
  t_frustum *frustum = &world->frustum;
  t_vec pScreen;
  float dx = (ix / (float) frame.width) - 0.5;
  float dy = (iy / (float) frame.height) - 0.5;
  vec_set(&pScreen, &camera->origin);
  vec_addScaled(&camera->forward, frustum->dz, &pScreen);
  vec_addScaled(&camera->right, dx * frustum->dw, &pScreen);
  vec_addScaled(&camera->up, -dy * frustum->dh, &pScreen);
  t_vec direction;
  vec_normalize(
    vec_sub(&pScreen, &camera->origin, &direction)
  );
  vec_set(&line->p0, &camera->origin);
  vec_set(&line->d, &direction);
}

