#ifndef SHADER_H
#define SHADER_H

#include "../scene/types.h"

__device__ void shader_labert(
  t_vec *diffuse, t_vec *specular, t_collision *collision, t_vec *I, t_vec *eye, t_vec *color
);

#endif
