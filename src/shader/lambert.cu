#include "shader.h"

__device__
void shader_labert(t_vec *diffuse, t_vec *specular, t_collision *collision, t_vec *I, t_vec *eye, t_vec *color) {
  t_vec *N = &collision->normal;
  t_vec *L = I;
  float LdotN = float_max(0, vec_dot(N, L));
  if (LdotN > 0) {
    t_vec *C = &collision->geometry->material.ks;
    color->x = LdotN * C->x * diffuse->x;
    color->y = LdotN * C->y * diffuse->y;
    color->z = LdotN * C->z * diffuse->z;
  }
}

