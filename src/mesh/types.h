#ifndef MESH_TYPES_H
#define MESH_TYPES_H

#include "../math/math3d.h"
#include "../material/types.h"

#define GEOM_NAME_LEN 16

typedef enum {SPHERE, TRIANGLE_MESH, PLANE} t_geometry_type;
typedef enum {NONE, AABB} t_bounds_type;

typedef struct {
  t_aabb aabb;
  t_bounds_type type;
} t_bounds;

typedef struct {
  int trisCount;
  int vsCount;
  int nsCount;
  float *vs;
  float *ns;
  int *idx_vs;
  int *idx_ns;
} t_triangle_mesh;

typedef struct {
  int id;
  char name[GEOM_NAME_LEN];
  t_geometry_type type;
    // TODO: delete sphere and plane
  t_sphere sphere;
  t_plane plane;
  t_triangle_mesh mesh;
  t_bounds bounds;
  t_material material;
  t_transform transform;
} t_geometry; // TODO: rename to t_node

__host__ __device__ t_geometry* geometry_printf(t_geometry *geometry);

#endif

