#include "types.h"

#include "../math/triangle.h"
#include "../scene/types.h"

__host__ __device__ void triangle_mesh_triangle_i(
  float *vs, int *idx_vs, int i, t_triangle *triangle
);

__host__ __device__ void triangle_mesh_vertex_i(t_vec *vx, float *vs, int i);

__device__ void triangle_mesh_normal_i(
  t_triangle_mesh *mesh, t_collision *collision
);


__host__
bool triangle_mesh_build(t_triangle_mesh *mesh, int trisCount, int vsCount, float *vs, float *ns, int *idx_vs, int *idx_ns) {
  if (vs == NULL || ns == NULL || idx_vs == NULL || idx_ns == NULL) {
    printf("[ERROR] Triangle mesh can't have null values.");
    return false;
  }
  if (trisCount <= 0) {
    printf("[ERROR] Triangle count must be a positive number.");
    return false;
  }
  if (vsCount <= 0 || vsCount % 3 != 0) {
    printf("[ERROR] Vertex count must be a positive multiple of 3.");
    return false;
  }
  mesh->trisCount = trisCount;
  mesh->vsCount = vsCount;
  mesh->vs = vs;
  mesh->ns = ns;
  mesh->idx_vs = idx_vs;
  mesh->idx_ns = idx_ns;
  return true;
}

__host__
void triangle_mesh_bounds_aabb(t_triangle_mesh *mesh, t_aabb *aabb) {
  float *vs = mesh->vs;
  t_vec v;
  triangle_mesh_vertex_i(&v, vs, 0);
  for (int i = 0; i < mesh->vsCount / 3; i++) {
    triangle_mesh_vertex_i(&v, vs, i);
    aabb->min.x = MIN(aabb->min.x, v.x);
    aabb->min.y = MIN(aabb->min.y, v.y);
    aabb->min.z = MIN(aabb->min.z, v.z);
    aabb->max.x = MAX(aabb->max.x, v.x);
    aabb->max.y = MAX(aabb->max.y, v.y);
    aabb->max.z = MAX(aabb->max.z, v.z);
  }
  printf("[INFO] AABB: [(%.3f, %.3f, %.3f), (%.3f, %.3f, %.3f)]\n",
    aabb->min.x, aabb->min.y, aabb->min.z, aabb->max.x, aabb->max.y, aabb->max.z
  );
}

__device__
bool triangle_mesh_intersection(t_triangle_mesh *mesh, t_line *line, t_collision *collision) {
  // FIXME: Inefficient approach
  t_collision icollision = {0};
  t_triangle triangle;
  int *idx_vs = mesh->idx_vs;
  float *vs = mesh->vs;
  bool hasClosestCollision = false;
  for (int i = 0; i < mesh->trisCount; i++) {
    triangle_mesh_triangle_i(vs, idx_vs, i, &triangle);
    bool hasCollision = triangle_intersection_line(&triangle, line, &icollision);
    if (hasCollision && (!hasClosestCollision || icollision.distance < collision->distance)) {
      hasClosestCollision = true;
      scene_copyCollisionValues(collision, &icollision);
      collision->index = i;
    }
  }
  if (hasClosestCollision) {
    triangle_mesh_normal_i(mesh, collision);
  }
  return hasClosestCollision;
}

__host__ __device__ 
void triangle_mesh_triangle_i(float *vs, int *idx_vs, int i, t_triangle *triangle) {
  i *= 3;
  triangle_mesh_vertex_i(&triangle->p0, vs, idx_vs[i]);
  triangle_mesh_vertex_i(&triangle->p1, vs, idx_vs[i + 1]);
  triangle_mesh_vertex_i(&triangle->p2, vs, idx_vs[i + 2]);
}

__host__ __device__ void triangle_mesh_vertex_i(t_vec *vx, float *vs, int i) {
  i *= 3;
  vx->x = vs[i];
  vx->y = vs[i + 1];
  vx->z = vs[i + 2];
}

__device__ void triangle_mesh_normal_i(t_triangle_mesh *mesh, t_collision *collision) {
  int triandleIndex = collision->index;
  // Triangle vertices
  t_triangle triangle;
  triangle_mesh_triangle_i(mesh->vs, mesh->idx_vs, triandleIndex, &triangle);
  // Normals for each vertex
  t_vec n1, n2, n3;
  int *idx_ns = mesh->idx_ns;
  float *ns = mesh->ns;
  int vertex_i = triandleIndex * 3;
  triangle_mesh_vertex_i(&n1, ns, idx_ns[vertex_i]);
  triangle_mesh_vertex_i(&n2, ns, idx_ns[vertex_i + 1]);
  triangle_mesh_vertex_i(&n3, ns, idx_ns[vertex_i + 2]);
  // Normal for collision
  t_vec f = collision->localPoint;
  t_vec p1 = triangle.p0;
  t_vec p2 = triangle.p1;
  t_vec p3 = triangle.p2;

  float f1x = p1.x - f.x;
  float f1y = p1.y - f.y;
  float f1z = p1.z - f.z;
  float f2x = p2.x - f.x;
  float f2y = p2.y - f.y;
  float f2z = p2.z - f.z;
  float f3x = p3.x - f.x;
  float f3y = p3.y - f.y;
  float f3z = p3.z - f.z;
  float a = vec_crossLen(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z, p1.x - p3.x, p1.y - p3.y, p1.z - p3.z);
  float a1 = vec_crossLen(f2x, f2y, f2z, f3x, f3y, f3z) / a;
  float a2 = vec_crossLen(f3x, f3y, f3z, f1x, f1y, f1z) / a;
  float a3 = vec_crossLen(f1x, f1y, f1z, f2x, f2y, f2z) / a;

  t_vec *n = &collision->normal;
  n->x = n1.x * a1 + n2.x * a2 + n3.x * a3;
  n->y = n1.y * a1 + n2.y * a2 + n3.y * a3;
  n->z = n1.z * a1 + n2.z * a2 + n3.z * a3;
}

