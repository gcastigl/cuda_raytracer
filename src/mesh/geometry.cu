#include "types.h"

#include "../math/transform.h"

#include <stdlib.h>

using namespace std;

__host__ __device__ t_geometry* geometry_printf(t_geometry *geometry) {
  printf("[DEBUG] %s | type: %d\n", geometry->name, geometry->type);
  t_triangle_mesh *mesh = &geometry->mesh;
  printf("[DEBUG] %s | transform (+ inverse):\n", geometry->name);
  matrix_printf(&geometry->transform.matrix);
  matrix_printf(&geometry->transform.inverse_matrix);
  printf("[DEBUG] %s | mesh.positions: %d\n", geometry->name, mesh->vsCount);
  floats_printf(mesh->vs, mesh->vsCount);
  printf("[DEBUG] %s | mesh.normals: %d\n", geometry->name, mesh->nsCount);
  floats_printf(mesh->ns, mesh->nsCount);
  printf("[DEBUG] %s | mesh.triangles: %d\n", geometry->name, mesh->trisCount);
  printf("[DEBUG] %s | mesh.indexes:\n", geometry->name);
  ints_printf(mesh->idx_vs, mesh->trisCount * 3);
  ints_printf(mesh->idx_ns, mesh->trisCount * 3);
  printf("[DEBUG] %s | geometry.bounds.type: %d\n", geometry->name, geometry->bounds.type);
  t_vec *min = &geometry->bounds.aabb.min;
  t_vec *max = &geometry->bounds.aabb.max;
  printf("[DEBUG] %s | geometry.bounds: (%.3f, %.3f, %.3f)x(%.3f, %.3f, %.3f)\n", 
    geometry->name, min->x, min->y, min->z, max->x, max->y, max->z
  );
  t_material *material = &geometry->material;
  printf("[DEBUG] %s | geometry.material.type: %d\n", geometry->name, material->type);
  t_vec *ks = &material->ks;
  t_vec *kd = &material->ks;
  printf("[DEBUG] %s | geometry.material(kd & ks): (%.3f, %.3f, %.3f) | (%.3f, %.3f, %.3f)\n", 
    geometry->name, ks->x, ks->y, ks->z, kd->x, kd->y,kd->z
  );
  return geometry;
}

