#ifndef MESH_TRIANGLE_H
#define MESH_TRIANGLE_H

#include "types.h"
#include "../math/line.h"
#include "../scene/types.h"

__host__ bool triangle_mesh_build(
  t_triangle_mesh *mesh, int trisCount, int vsCount, 
  float *vs, float *ns, int *inx_vs, int *inx_ns
);

__host__ void triangle_mesh_bounds_aabb(t_triangle_mesh *mesh, t_aabb *aabb);

__device__ bool triangle_mesh_intersection(
  t_triangle_mesh *mesh, t_line *line, t_collision *collision
);


#endif

