#ifndef MAKEPNG_H
#define MAKEPNG_H

#include "types.h"

int writeImage(
  const char* filename, int width, int height, pixel_t *buffer, const char* title
);

#endif

