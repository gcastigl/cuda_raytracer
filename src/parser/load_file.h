#ifndef PARESER_COLLADA_H
#define PARESER_COLLADA_H

#include "../scene/types.h"
#include <iostream>
#include <sstream>
#include <cstring>

__host__ bool parser_fromColladaFile(t_world *world, std::string filename);

#endif

