#include "parser.h"
#include "pugixml.hpp"
#include "load_file.h"

#include "../math/plane.h"
#include "../math/transform.h"
#include "../material/types.h"
#include "../mesh/triangle_mesh.h"
#include "../util/strings.h"

#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

__host__ bool scene_setup(pugi::xml_document & doc, t_world* world, t_dmemory *device);
__host__ void parser_set_transform(t_transform *transform, float dx, float dy, float dz);
__host__ void addMeshes(t_scene *scene, t_dmemory *device);
__host__ void addLights(t_scene *scene, t_dmemory *device);
__host__ void copyMeshAndLightsToDevice(t_scene *scene, t_dmemory *device);

__host__ bool parser_load(char *sceneFileName, t_world *world, t_frame *frame, t_dmemory *device, t_options *options) {
  string sceneFileString(sceneFileName);
  cout << "[INFO] Parsing scene from: '" << sceneFileString << "'" << endl;
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(sceneFileName);
  if (!result) {
    cout << "[ERROR] File '" + sceneFileString + "' was not found or has an invalid XML format" << endl;
    return false;
  }
  pugi::xpath_query query_resolution_width("/render/config/resolution/width");
  pugi::xpath_query query_resolution_height("/render/config/resolution/height");
  pugi::xpath_query query_threads("/render/config/threads");
  pugi::xpath_query query_renderMode("/render/config/render_mode");
  bool sceneLoaded = scene_setup(doc, world, device);
  if (!sceneLoaded) {
    cout << "[ERROR] Scene could not be loaded" << endl;
    return false;
  }
  frame->width = query_resolution_width.evaluate_number(doc);
  frame->height = query_resolution_height.evaluate_number(doc);
  frame->startIndex = 0;
  frame->endIndex = frame->width * frame->height;
  options->threads = query_threads.evaluate_number(doc);
  options->renderMode = (t_render_mode) query_renderMode.evaluate_number(doc);
  cout << "[DEBUG] Frame width, height: (" << frame->width << " x " << frame->height << ")" << endl;
  pugi::xpath_query query_debug("/render/config/debug");
  pugi::xml_node pugi_debug = query_debug.evaluate_node(doc).node();
  if (pugi_debug && pugi_debug.text().as_int() != 0) {
    cout << "[DEBUG] Threads: " << options->threads << endl;
    for (int i = 0; i < world->scene.geometriesCount; i++) {
      printf("\n\n");
      geometry_printf(world->scene.geometries + i);
    }
  }
  printf("[INFO] Parsing scene [OK]\n");
  return true;
}

__host__ bool scene_setup(pugi::xml_document & doc, t_world* world, t_dmemory *device) {
  // Camera location
  t_camera *camera = &world->camera;
  pugi::xpath_query query_camera_position("/render/camera/position");
  pugi::xpath_query query_camera_forward("/render/camera/forward");
  pugi::xpath_query query_camera_up("/render/camera/up");
  pugi::xpath_query query_camera_right("/render/camera/right");
  readVector3f(query_camera_position.evaluate_string(doc), &camera->origin);
  readVector3f(query_camera_forward.evaluate_string(doc), &camera->forward);
  readVector3f(query_camera_up.evaluate_string(doc), &camera->up);
  readVector3f(query_camera_right.evaluate_string(doc), &camera->right);
  // Frustrum size
  world->frustum.dz = 1;
  world->frustum.dw = 1;
  world->frustum.dh = 0.5;
  // Scene
  pugi::xpath_query query_collada_input("/render/input");
  string filename = query_collada_input.evaluate_string(doc);
  bool success = parser_fromColladaFile(world, filename);
  if (success) {
    copyMeshAndLightsToDevice(&world->scene, device);
  }
  return success;
}

__host__ void copyMeshAndLightsToDevice(t_scene *scene, t_dmemory *device) {
  // Copy geometries
  int geometriesSize = scene->geometriesCount * sizeof(t_geometry);
  t_geometry *d_geometries = (t_geometry*) malloc(geometriesSize);
  memcpy(d_geometries, scene->geometries, geometriesSize);
  for (int i = 0; i < scene->geometriesCount; i++) {
    t_geometry *d_geometry = d_geometries + i;
    if (d_geometry->type == TRIANGLE_MESH) {
      cout << "[DEBUG] Copying '" << d_geometry->name << "' to device" << endl;
      t_triangle_mesh *d_mesh = &d_geometry->mesh;
      int indexesSize = d_mesh->trisCount * sizeof(int) * 3;
      int vsSize = d_mesh->vsCount * sizeof(float);
      int nsSize = d_mesh->nsCount * sizeof(float);
      cudaMalloc((void **) &d_mesh->idx_vs, indexesSize);
      cudaMalloc((void **) &d_mesh->idx_ns, indexesSize);
      cudaMalloc((void **) &d_mesh->vs, vsSize);
      cudaMalloc((void **) &d_mesh->ns, nsSize);
      t_triangle_mesh *mesh = &scene->geometries[i].mesh;
      cudaMemcpy(d_mesh->idx_vs, mesh->idx_vs, indexesSize, cudaMemcpyHostToDevice);
      cudaMemcpy(d_mesh->idx_ns, mesh->idx_ns, indexesSize, cudaMemcpyHostToDevice);
      cudaMemcpy(d_mesh->vs, mesh->vs, vsSize, cudaMemcpyHostToDevice);
      cudaMemcpy(d_mesh->ns, mesh->ns, nsSize, cudaMemcpyHostToDevice);
      // TODO: free host mesh?
    }
  }
  cudaMalloc((void **) &device->geometries, geometriesSize);
  cudaMemcpy(device->geometries, d_geometries, geometriesSize, cudaMemcpyHostToDevice); 
  free(d_geometries);
  // Copy lights
  cout << "[DEBUG] Copying lights to device" << endl;
  int lightsSize = scene->lightsCount * sizeof(t_light);
  cudaMalloc((void **) &device->lights, lightsSize);
  cudaMemcpy(device->lights, scene->lights, lightsSize, cudaMemcpyHostToDevice); 
}

