#ifndef PARSER_H
#define PARSER_H

#include "../scene/types.h"

typedef struct {
  t_world *world;
  t_collision *collisions;
  t_geometry *geometries;
  t_light *lights;
} t_dmemory;

__host__ bool parser_load(
  char *sceneFileName, t_world *world, t_frame *frame, t_dmemory *device, t_options *options
);

#endif

