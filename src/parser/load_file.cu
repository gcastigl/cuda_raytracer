#include "pugixml.hpp"

#include "../util/strings.h"
#include "../scene/types.h"
#include "../mesh/types.h"
#include "../mesh/triangle_mesh.h"
#include "../math/transform.h"
#include "../material/types.h"

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

bool parseGeometries(pugi::xml_document &doc, string nodeId, t_geometry *geometry);
bool parseLight(pugi::xml_document &doc, string nodeId, t_light *light, string transformString);

// http://pugixml.org/docs/manual.html
bool parser_fromColladaFile(t_world *world, string filename) {
  cout << "[INFO] Parsing file: '" + filename + "'" << endl;
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(filename.c_str());
  if (!result) {
    cout << "[ERROR] File '" + filename + "' was not found or has an invalid format" << endl;  
    return false;
  }
  pugi::xpath_query geometries_count_query("count(//visual_scene/node/instance_geometry)");
  world->scene.geometriesCount = geometries_count_query.evaluate_number(doc);
  pugi::xpath_query lights_count_query("count(//visual_scene/node/instance_light)");
  world->scene.lightsCount = lights_count_query.evaluate_number(doc);
  cout << "[DEBUG] " << "Total geometries: " << world->scene.geometriesCount << endl;
  cout << "[DEBUG] " << "Total lights: " << world->scene.lightsCount << endl;  
  world->scene.geometries = new t_geometry[world->scene.geometriesCount];
  world->scene.lights = new t_light[world->scene.lightsCount];
  int geometryIndex = 0;
  int lightIndex = 0;
  pugi::xpath_node_set pugi_nodes = doc.select_nodes("//visual_scene/node[@type='NODE']");
  for (pugi::xpath_node_set::const_iterator it = pugi_nodes.begin(); it != pugi_nodes.end(); ++it) {
    pugi::xpath_node pugi_node = *it;
    string nodeId = pugi_node.node().attribute("id").value();
    // FIXME: add support for translate, rotateY, rotateX, rotateZ & scale
    string transformString = pugi_node.node().child("matrix").text().get();
    pugi::xml_node pugi_geometry = pugi_node.node().child("instance_geometry");
    pugi::xml_node pugi_light = pugi_node.node().child("instance_light");
    pugi::xml_node pugi_camera = pugi_node.node().child("instance_camera");
    if (pugi_geometry) {
      t_geometry *geometry = world->scene.geometries + geometryIndex++;
      memcpy(geometry->name, nodeId.c_str(), sizeof(char) * nodeId.length() + 1);  // TODO: carefull here
      string meshId = pugi_geometry.attribute("url").value();
      bool success = parseGeometries(doc, meshId.substr(1, meshId.size()), geometry);
      if (!success) {
        printf("[ERROR] Could not parse geometry: %s\n", geometry->name);
        return false;
      }
      readNFloats(transformString, geometry->transform.matrix.values, 16);
      transform_update_inverse(&geometry->transform);
      cout << "[DEBUG] Transform: " << transformString << endl;
    } else if (pugi_light) {
      t_light *light = world->scene.lights + lightIndex++;
      string lightId = pugi_light.attribute("url").value();
      lightId = lightId.substr(1, lightId.size());
      parseLight(doc, lightId, light, transformString);
    } else if (pugi_camera) {
      cout << "[WARN] Camera needs to work using transforms" << endl;
    } else {
      cout << "[WARN] Unknown type for:" + nodeId << endl;
    }
  }
  return true;
}

bool parseGeometries(pugi::xml_document &doc, string nodeId, t_geometry *geometry) {
  cout << "[INFO] Parsing geometry '" << nodeId << "'" << endl;
  pugi::xpath_query geometry_query(("//geometry[@id='" + nodeId + "']").c_str());
  pugi::xml_node geometryNode = geometry_query.evaluate_node(doc).node();
  if (!geometryNode) {
    cout << "[ERROR] Missing geometry information for " << nodeId << endl;
    return false;
  }
  t_triangle_mesh *mesh = &geometry->mesh;
  pugi::xpath_query polylist_query(".//polylist");
  pugi::xml_node polylist = polylist_query.evaluate_node(geometryNode).node();
  int polylistCount = polylist.attribute("count").as_int();
  pugi::xpath_query input_count("count(.//input)");
  pugi::xpath_query input_vertex_query(".//input[@semantic='VERTEX']");
  pugi::xpath_query input_normal_query(".//input[@semantic='NORMAL']");
  pugi::xpath_query input_texture_query(".//input[@semantic='TEXCOORD']");
  int inputCount = input_count.evaluate_number(polylist);
  int vertexOffset = input_vertex_query.evaluate_node(polylist).node().attribute("offset").as_int();
  int normalOffset = input_normal_query.evaluate_node(polylist).node().attribute("offset").as_int();
  int textureOffset = input_texture_query.evaluate_node(polylist).node().attribute("offset").as_int();
  // TODO: assuming vcount to always be equals to 3
  int psCount = inputCount * polylistCount * 3;
  string psString = polylist.child("p").text().get();
  int *ps = new int[psCount];  
  readNInts(psString, ps, psCount);
  // Mesh indexes
  mesh->trisCount = polylistCount;
  mesh->idx_vs = new int[polylistCount * 3];
  mesh->idx_ns = new int[polylistCount * 3];
  int vsIndex = 0;
  int nsIndex = 0;
  for (int i = 0; i < psCount; i++) {
    int mod = i %  inputCount;
    if (mod == vertexOffset) {
      mesh->idx_vs[vsIndex++] = ps[i];
    } else if (mod == normalOffset) {
      mesh->idx_ns[nsIndex++] = ps[i];
    }
  }
  delete [] ps;
  // positions
  pugi::xpath_query positions_query((".//float_array[@id='" + nodeId + "-positions-array']").c_str());
  pugi::xml_node positions_values = positions_query.evaluate_node(geometryNode).node();
  if (!positions_values) {
    cout << "[ERROR] Positions node not found" << endl;
    return false;
  }
  int positionsCount = positions_values.attribute("count").as_int();
  string positionsString = positions_values.text().get();
  mesh->vs = new float[positionsCount];
  mesh->vsCount = positionsCount;
  readNFloats(positionsString, mesh->vs, positionsCount);
  // normals
  pugi::xpath_query normals_query((".//float_array[@id='" + nodeId + "-normals-array']").c_str());
  pugi::xml_node normals_values = normals_query.evaluate_node(geometryNode).node();
  if (!normals_values) {
    cout << "[ERROR] Normals node not found" << endl;
    return false;
  }
  int normalsCount = normals_values.attribute("count").as_int();
  string normalsString = normals_values.text().get();
  mesh->ns = new float[normalsCount];
  mesh->nsCount = normalsCount;
  readNFloats(normalsString, mesh->ns, normalsCount);
  geometry->type = TRIANGLE_MESH;
  cout << "[DEBUG] " << nodeId << " positions: " << mesh->vsCount << endl;
  cout << "[DEBUG] " << nodeId << " normals: " << mesh->nsCount << endl;
  cout << "[DEBUG] " << nodeId << " triangles: " << mesh->trisCount << endl;
  cout << "[DEBUG] " << nodeId << " type: " << geometry->type << endl;
  // Materials
  t_material *material = &geometry->material;
  material_init(material, MATTE);
  pugi::xml_attribute pugi_material = polylist.attribute("material");
  if (pugi_material) {
    string materialId = pugi_material.value();
    cout << "[DEBUG] Loading material: " << materialId << endl;
    pugi::xpath_query material_query(("//material[@id='" + materialId + "']/instance_effect/@url").c_str());
    string materialEffectUrl = material_query.evaluate_string(doc);
    materialEffectUrl = materialEffectUrl.substr(1, materialEffectUrl.size());
    pugi::xpath_query effect_query(("//library_effects/effect[@id='" + materialEffectUrl + "']/profile_COMMON").c_str());
    pugi::xml_node pugi_effect = effect_query.evaluate_node(doc).node();
    pugi::xml_node pugi_technique = pugi_effect.child("technique");
    pugi::xml_node pugi_phong = pugi_technique.child("phong");
    pugi::xml_node pugi_lambert = pugi_technique.child("lambert");
    if (pugi_phong) {
      cout << "[DEBUG] '" << materialId << "' uses Phong shader." << endl;
      material->shaderType = SHADER_PHONG;
      string emissionString = pugi_phong.child("emission").child("color").text().get();
      string ambientString = pugi_phong.child("ambient").child("color").text().get();
      string specularString = pugi_phong.child("specular").child("color").text().get();
      readVector3f(emissionString, &material->ke);
      readVector3f(ambientString, &material->ka);
      readVector3f(specularString, &material->ks);
      material->shininess = pugi_phong.child("shininess").child("float").text().as_float();
      material->refractionIndex = pugi_phong.child("index_of_refraction").child("float").text().as_float();
      pugi::xml_node pugi_diffuse = pugi_phong.child("diffuse");
      pugi::xml_node pugi_diffuse_color = pugi_diffuse.child("color");
      pugi::xml_node pugi_diffuse_texture = pugi_diffuse.child("texture");
      if (pugi_diffuse_color) {
        string diffuseString = pugi_diffuse_color.text().get();
        readVector3f(diffuseString, &material->kd);
      } else {
        cout << "[ERROR] Textures still not implemented [IGNORED]" << endl;
      }
    } else if (pugi_lambert) {
      cout << "[DEBUG] Material: '" << materialId << "' uses Lambert shader." << endl;
      material->shaderType = SHADER_LAMBERT;
      string ambientString = pugi_phong.child("ambient").child("color").text().get();
      string specularString = pugi_phong.child("specular").child("color").text().get();
      readVector3f(ambientString, &material->ka);
      readVector3f(specularString, &material->ks);
    } else {
      cout << "[ERROR] Unknown shader type for material: " << materialId << endl;
      return false;
    }
  }
  geometry->bounds.type = AABB;
  triangle_mesh_bounds_aabb(mesh, &geometry->bounds.aabb);
  cout << "[INFO] Parsing geometry '" << nodeId << "' [OK]" << endl;
  return true;
}

bool parseLight(pugi::xml_document &doc, string nodeId, t_light *light, string transformString) {
  cout << "[INFO] Parsing light '" << nodeId << "'" << endl;
  pugi::xpath_query light_query(("//light[@id='" + nodeId + "']").c_str());
  
  pugi::xml_node pugi_light = light_query.evaluate_node(doc).node();
  if (!pugi_light) {
    cout << "[ERROR] light node '" << nodeId << "' was not found" << endl;
    return false;
  }
  pugi::xml_node pugi_technique = pugi_light.child("technique_common");
  pugi::xml_node pugi_point = pugi_technique.child("point");
  pugi::xml_node pugi_ambient = pugi_technique.child("ambient");
  // TODO: implement this light types as well
  // pugi::xml_node pugi_directional = pugi_technique.child("directional");
  // pugi::xml_node pugi_spot = pugi_technique.child("spot");
  if (pugi_point) {
    cout << "[DEBUG] Light type: Point" << endl;
    light->type = POINT;
    string colorString = pugi_point.child("color").text().get();
    readVector3f(colorString, &light->specular);
    readVector3f(colorString, &light->diffuse);
    light->k0 = pugi_point.child("constant_attenuation").text().as_float();
    light->k1 = pugi_point.child("linear_attenuation").text().as_float();
    light->k2 = pugi_point.child("quadratic_attenuation").text().as_float();
    float values[16];
    readNFloats(transformString, values, 16);
    light->p0.x = values[3];
    light->p0.y = values[7];
    light->p0.z = values[11];
    cout << "[DEBUG] Color: " << colorString << endl;
    cout << "[DEBUG] k0, k1, k2: " 
      << light->k0 << "|" << light->k1 << "|" << light->k2 << endl
    ;
    t_vec p0 = light->p0;
    cout << "[DEBUG] p0: " << p0.x << "|" << p0.y << "|" << p0.z << endl;
  } else if (pugi_ambient) {
    cout << "[DEBUG] Light type: Ambient" << endl;
    light->type = AMBIENT;
    string colorString = pugi_ambient.child("color").text().get();
    readVector3f(colorString, &light->specular);
    readVector3f(colorString, &light->diffuse);
    cout << "[DEBUG] Color: " << colorString << endl;
  } else {
    cout << "[ERROR] Unknown Light type" << endl;
    return false;
  }
  cout << "[INFO] Parsing light '" << nodeId << "' [OK]" << endl;
  return true;
}

