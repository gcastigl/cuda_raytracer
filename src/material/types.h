#ifndef MATERIAL_TYPES_H
#define MATERIAL_TYPES_H

#include "../math/math3d.h"

#define MAT_NAME_LEN 16

typedef enum {MATTE, MIRROR, METAL, GLASS} t_meterial_type;
typedef enum {SHADER_LAMBERT, SHADER_PHONG} t_shader_type;

typedef struct {
  char name[MAT_NAME_LEN];
  t_meterial_type type;
  t_shader_type shaderType;
  
  /*
  * Specular reflection constant, the ratio of reflection of the specular
  * term of incoming light.
  */
  t_vec ks;

  /*
  * Diffuse reflection constant, the ratio of reflection of the diffuse term
  * of incoming light (Lambertian reflectance).
  */
  t_vec kd;

  /*
  * Ambient reflection constant, the ratio of reflection of the ambient term
  * present in all points in the scene rendered.
  */
  t_vec ka;

  /* The reflectivity color of the material */
  t_vec kt;

  /* Fraction of light transmitted through the surface */
  t_vec kr;

  /* Emission reflection constant. */
  t_vec ke;

  /**
  * (Phong) constant which is larger for surfaces that
  * are smoother and more mirror-like. When this constant is large the
  * specular highlight is small.
  */
  float shininess;

  float refractionIndex;
} t_material;

__host__ void material_init(t_material *material, t_meterial_type type);

#endif

