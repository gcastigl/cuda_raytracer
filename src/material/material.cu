#include "types.h"

__host__
void material_init(t_material *material, t_meterial_type type) {
  material->type = type;
  vec_setAll(&material->ks, 1, 1, 1);
  vec_setAll(&material->kd, 1, 1, 1);
  vec_setAll(&material->ka, 1, 1, 1);
  vec_setAll(&material->kt, 1, 1, 1);
  vec_setAll(&material->kr, 1, 1, 1);
}

