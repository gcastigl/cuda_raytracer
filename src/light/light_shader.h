#ifndef LIGHT_SHADER_H
#define LIGHT_SHADER_H

#include "types.h"
#include "../scene/types.h"

__device__
void light_shader_apply(t_light *light, t_scene *scene, t_collision *collision, t_vec *eye, t_vec *color);

#endif

