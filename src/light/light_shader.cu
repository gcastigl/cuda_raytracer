#include "light_shader.h"
#include "../math/line.h"
#include "../shader/shader.h"
#include "../scene/scene_traverser.h"

__device__ void light_point_apply(t_light *light, t_scene *scene, t_collision *collision, t_vec *eye, t_vec *color);
__device__ bool light_isCulled(t_light *light, t_scene *scene, t_vec *p0, t_vec *direction, t_geometry *geomToIgnore);
__device__ void light_ambient_apply(t_light *light, t_scene *scene, t_collision *collision, t_vec *eye, t_vec *color);

__device__
void light_shader_apply(t_light *light, t_scene *scene, t_collision *collision, t_vec *eye, t_vec *color) {
  if (light->type == POINT) {
    light_point_apply(light, scene, collision, eye, color);
  } else if (light->type == AMBIENT) {
    light_ambient_apply(light, scene, collision, eye, color);
  } else {
    printf("UNKNOWN LIGHT: %d\n", light->type);
  }
}

__device__
void light_point_apply(t_light *light, t_scene *scene, t_collision *collision, t_vec *eye, t_vec *color) {
  if (vec_dot(&collision->normal, eye) < 0) {
    return;
  }
  t_vec *position = &collision->worldPoint;
  t_vec direction;
  vec_set(&direction, &light->p0);
  vec_sub(&direction, position);
  float distance = vec_normalize(&direction);  
  if (light_isCulled(light, scene, position, &direction, collision->geometry)) {
    return;
  }
  shader_labert(
    &light->specular, &light->diffuse, collision, &direction, eye, color
  );
  float decay = light->k0 + light->k1 * distance + light->k2 * (distance * distance);
  vec_scale(color, 1 / decay);
}

__device__
bool light_isCulled(t_light *light, t_scene *scene, t_vec *p0, t_vec *direction, t_geometry *geomToIgnore) {
  t_line line;
  line_set(&line, p0, direction);
  return scene_traverser_isCulled(
    scene, &line, vec_distanceSq(p0, &light->p0), geomToIgnore
  );
}

__device__
void light_ambient_apply(t_light *light, t_scene *scene, t_collision *collision, t_vec *eye, t_vec *color) {
  vec_set(color, &light->diffuse);
  t_material *material = &collision->geometry->material;
  vec_mult(color, &material->ka);
}

