#include "types.h"

t_light* light_init(t_light *light, t_light_type type) {
  light->type = type;
  vec_setAll(&light->specular, 1, 1, 1);
  vec_setAll(&light->diffuse, 1, 1, 1);
  return light;
}

t_light* light_point(t_light *light, t_vec *p0) {
  light_init(light, POINT);
  vec_set(&light->p0, p0);
  light->k0 = 1;
  light->k1 = 1;
  light->k2 = 0;
  return light;
}

t_light* light_ambient(t_light *light, t_vec *ambient) {
  light_init(light, AMBIENT);
  vec_set(&light->diffuse, ambient);
  return light;  
}

