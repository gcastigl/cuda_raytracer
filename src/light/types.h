#ifndef LIGHT_TYPES_H
#define LIGHT_TYPES_H

#include "../math/math3d.h"

typedef enum {AMBIENT, POINT} t_light_type;

typedef struct {
  t_light_type type;
	t_vec specular;
  t_vec diffuse;
  // Pointlight
  t_vec p0;
  float k0; // Constant Attenuation
  float k1; // Linear Attenuation
  float k2; // Quadratic ​Attenuation
} t_light;

t_light* light_init(t_light *light, t_light_type type);

t_light* light_point(t_light *light, t_vec *p0);

t_light* light_ambient(t_light *light, t_vec *ambient);

#endif

