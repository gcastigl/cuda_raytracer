#include "triangle.h"

__device__ void triangle_normal(t_vec *vap, t_vec *vbp, t_vec *vcp, t_vec *normal);

__device__ 
bool triangle_intersection_line(t_triangle *triangle, t_line *line, t_collision *collision) {
  return triangle_intersection_line_points(
    &triangle->p0, &triangle->p1, &triangle->p2, line, collision
  );
}

__device__ 
bool triangle_intersection_line_points(t_vec *vap, t_vec *vbp, t_vec *vcp, t_line *line, t_collision *collision) {
  bool hasCollision = triangle_intersection_line_points_dist(vap, vbp, vcp, line, &collision->distance);
  if (hasCollision) {
    line_pointAt(line, collision->distance, &collision->localPoint);
  }
  return hasCollision;
}

__device__ 
bool triangle_intersection_line_points_dist(t_vec *vap, t_vec *vbp, t_vec *vcp, t_line *line, float *dist) {
  t_vec va = *vap;
  t_vec vb = *vbp;
  t_vec vc = *vcp;
  float a = va.x - vb.x;
  float b = va.x - vc.x;
  float c = line->d.x;
  float d = va.x - line->p0.x;
  float e = va.y - vb.y;
  float f = va.y - vc.y;
  float g = line->d.y;
  float h = va.y - line->p0.y;
  float i = va.z - vb.z;
  float j = va.z - vc.z;
  float k = line->d.z;
  float l = va.z - line->p0.z;
  float m = f * k - g * j;
  float n = h * k - g * l;
  float p = f * l - h * j;
  float q = g * i - e * k;
  float s = e * j - f * i;
  float inv = 1 / (a * m + b * q + c * s);
  float aux1 = d * m - b * n - c * p;
  float beta = aux1 * inv;
  if (beta < 0) {
    return false;
  }
  float r = e * l - h * i;
  float aux2 = a * n + d * q + c * r;
  float gamma = aux2 * inv;
  if (gamma < 0) {
    return false;
  }
  if (beta + gamma > 1) {
    return false;
  }
  float aux3 = a * p - b * r + d * s;
  float t = aux3 * inv;
  if (t < 0) {
    return false;
  }
  *dist = t;
  return true;
}

__device__ void triangle_normal(t_vec *vap, t_vec *vbp, t_vec *vcp, t_vec *normal) {
  t_vec dab, dac;
  vec_set(&dab, vbp);
  vec_sub(&dab, vap);
  
  vec_set(&dac, vcp);
  vec_sub(&dac, vap);

  vec_cross(normal, &dac, &dab);
  vec_normalize(normal);
}

