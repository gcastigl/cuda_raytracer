#include "sphere.h"

__device__ 
bool sphere_intersection_line_dist(t_sphere *sphere, t_line *line, t_collision *collision) {
  t_vec *o = &line->p0;
  t_vec *l = &line->d;
  t_vec *c = &sphere->c;
  float r = sphere->r;
  t_vec oc;
  vec_sub(o, c, &oc);
  float lDotOC = vec_dot(l, &oc);
  float ocLen = vec_modSq(&oc);
  float nSq = (lDotOC * lDotOC) - vec_modSq(&oc) + r * r;
  if (nSq < 0) {
    return false;
  }
  float d;
  if (float_isZero(nSq)) {
    d = -lDotOC;
  } else {
    float n = sqrt(nSq);
    float d1 = -lDotOC + n;
    float d2 = -lDotOC - n;
    d = min(d1, d2);
  }
  collision->distance = d;
  line_pointAt(line, d, &collision->localPoint);
  sphere_normal(sphere, collision);
  return true;
}

__device__ void sphere_normal(t_sphere *sphere, t_collision *collision) {
  t_vec op;
  vec_sub(&collision->localPoint, &sphere->c, &op);
  vec_scale(&op, (1.0 / sphere->r));
  vec_set(&collision->normal, &op);
}

