#include "line.h"
#include "transform.h"

__device__ t_line* line_set(t_line *line, t_vec *p0, t_vec *direction) {
  vec_set(&line->p0, p0);
  vec_set(&line->d, direction);
  return line;
}

__device__ void line_pointAt(t_line *line, float d, t_vec *point) {
  line_pointAtFromPoint(&line->p0, &line->d, d, point);
}

__device__
void line_pointAtFromPoint(t_vec *o, t_vec *l, float d, t_vec *point) {
    vec_set(point, o);
    vec_addScaled(l, d, point);
}

__device__
void line_transform_inverse(t_line *line, t_transform *transform, t_line *result, float *scale) {
  transform_inverse_point(transform, &line->p0, &result->p0);
  transform_inverse_direction(transform, &line->d, &result->d);
  *scale = vec_mod(&result->d);
  vec_normalize(&result->d);
}


