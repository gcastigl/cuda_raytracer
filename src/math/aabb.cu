#include "aabb.h"

__host__ void aabb_create(t_aabb *aabb, t_vec *min, t_vec *max) {
  vec_set(&aabb->min, min);
  vec_set(&aabb->max, max);
}

__device__ bool aabb_intersects(t_aabb *aabb, t_line *line) {
  t_vec min = aabb->min;
  t_vec max = aabb->max;
  t_vec dir = line->d;
  t_vec orig = line->p0;
  float tmin = (min.x - orig.x) / dir.x;
  float tmax = (max.x - orig.x) / dir.x;
  if (tmin > tmax) { 
    swap(tmin, tmax);
  }
  float tymin = (min.y - orig.y) / dir.y;
  float tymax = (max.y - orig.y) / dir.y;
  if (tymin > tymax) {
    swap(tymin, tymax);
  }
  if ((tmin > tymax) || (tymin > tmax)) {
    return false;
  }
  if (tymin > tmin) {
    tmin = tymin;
  }
  if (tymax < tmax) {
    tmax = tymax;
  }
  float tzmin = (min.z - orig.z) / dir.z;
  float tzmax = (max.z - orig.z) / dir.z;
  if (tzmin > tzmax) {
    swap(tzmin, tzmax);
  }
  if ((tmin > tzmax) || (tzmin > tmax)) {
    return false;
  }
  if (tzmin > tmin) {
    tmin = tzmin;
  }
  if (tzmax < tmax) {
    tmax = tzmax;
  }
  return true; 
}

