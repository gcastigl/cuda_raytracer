#ifndef MATH3D_H
#define MATH3D_H

#include <iostream>
#include <stdio.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif 

#define MAX(X, Y) ((X) < (Y) ? (Y) : (X))
#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))

typedef struct {
	float values[9];
} t_matrix3f;

typedef struct {
	float values[16];
} t_matrix4f;

typedef struct {
	t_matrix4f matrix;
	t_matrix4f inverse_matrix;
} t_transform;

typedef struct {
  float x;
  float y;
  float z;
} t_vec;

typedef struct {
	t_vec p0;
	t_vec p1;
	t_vec p2;
} t_triangle;

typedef struct {
	t_vec p0;
	t_vec d;
} t_line;

typedef struct {
  t_vec c;
  float r;
} t_sphere;

typedef struct {
	t_vec p0;
	t_vec n;
	t_vec du;
	t_vec dv;
	float uLen;
  float vLen;
} t_plane;

typedef struct {
  t_vec min;
  t_vec max;
} t_aabb;

// Floats 

__device__ void swap(float& a, float& b);

__device__ float float_max(float n1, float n2);

__device__ float float_sq(float f);

__host__ __device__ float float_eq(float n1, float n2);

__host__ __device__ float float_isZero(float n1);

__device__ float float_lenghtSq(float x, float y, float z);

__host__ __device__ float* floats_printf(float *ns, int count);

__host__ __device__ int* ints_printf(int *ns, int count);

// Vector

__device__ bool vec_equals(t_vec *p1, t_vec *p2);

__host__ __device__ t_vec* vec_printf(t_vec *p);

__host__ __device__ void vec_printf_2(t_vec *p1, t_vec *p2);

__host__ __device__ t_vec* vec_set(t_vec *target, t_vec *source);

__device__ t_vec* vec_scale(t_vec *target, float scale);

__device__ t_vec* vec_add(t_vec *target, t_vec *add);

__device__ t_vec* vec_sub(t_vec *target, t_vec *substract);

__device__ t_vec* vec_mult(t_vec *target, t_vec *mult);

__device__ float vec_distanceSq(t_vec *v1, t_vec *v2);

__host__ __device__ t_vec* vec_setAll(t_vec *target, float x, float y, float z);

__host__ __device__ float vec_dot(t_vec *p1, t_vec *p2);

__device__ float vec_dot_float(t_vec *p1, float x, float y, float z);

__host__ __device__ float vec_modSq(t_vec *p);

__host__ __device__ float vec_mod(t_vec *p);

__host__ __device__ float vec_normalize(t_vec *vec);

__device__ t_vec* vec_sub(t_vec *p1, t_vec *p2, t_vec *target);

__host__ __device__ t_vec* vec_addScaled(t_vec *add, float scale, t_vec *target);

__device__ float vec_crossLen(float x1, float y1, float z1, float x2, float y2, float z2);

__host__ __device__ t_vec* vec_cross(t_vec *cross, t_vec *a, t_vec *b);

#endif

