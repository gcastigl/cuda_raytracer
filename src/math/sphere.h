#ifndef SPHERE_H
#define SPHERE_H

#include "math3d.h"
#include "line.h"
#include "../scene/types.h"

__device__ 
bool sphere_intersection_line_dist(t_sphere *sphere, t_line *line, t_collision *collision);

__device__ void sphere_normal(t_sphere *sphere, t_collision *collision);

#endif

