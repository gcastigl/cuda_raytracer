#ifndef MATH_TRANSFORM_H
#define MATH_TRANSFORM_H

#include "math3d.h"

__host__ __device__ void transform_id(t_transform *tansform);

__host__ bool transform_set(t_transform *tansform, const float values[16]);

__host__ void transform_translate(t_transform *tansform, float dx, float dy, float dz);

__host__ bool transform_update_inverse(t_transform *tansform);

__device__ void transform_inverse_point(t_transform *tansform, t_vec *point, t_vec *result);

__device__ void transform_inverse_direction(t_transform *tansform, t_vec *direction, t_vec *result);

__device__ void transform_direction(t_transform *tansform, t_vec *direction, t_vec *result);

__device__ void transform_point(t_transform *transform, t_vec *point, t_vec *result);

__device__ __host__ void matrix_printf(t_matrix4f *matrix);

__host__ __device__ t_matrix4f* matrix_mult(t_matrix4f *m1, t_matrix4f *m2, t_matrix4f *result);

#endif

