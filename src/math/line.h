#ifndef LINE_H
#define LINE_H

#include "math3d.h"

__device__ t_line* line_set(t_line *line, t_vec *p0, t_vec *direction);

__device__ void line_pointAt(t_line *line, float d, t_vec *point);

__device__ void line_pointAtFromPoint(t_vec *o, t_vec *l, float d, t_vec *point);

__device__ void line_transform_inverse(
  t_line *line, t_transform *transform, t_line *result, float *scale
);

#endif

