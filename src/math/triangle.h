#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "line.h"
#include "math3d.h"
#include "../scene/types.h"

__device__ 
bool triangle_intersection_line(t_triangle *triangle, t_line *line, t_collision *collision);

__device__ 
bool triangle_intersection_line_points(t_vec *vap, t_vec *vbp, t_vec *vcp, t_line *line, t_collision *collision);

__device__ 
bool triangle_intersection_line_points_dist(t_vec *vap, t_vec *vbp, t_vec *vcp, t_line *line, float *dist);

#endif

