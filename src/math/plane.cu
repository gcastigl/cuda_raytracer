#include "plane.h"
#include "triangle.h"
#include "line.h"

__host__ void setupUV(t_plane *plane, t_vec *du, t_vec *dv);
__device__ bool infinite_plane_intersection(t_plane *plane, t_line *line, t_collision *collision);
__device__ float plane_t1(t_plane *plane, t_vec *p0);

__host__
void plane_create(t_plane *plane, t_vec *p0, t_vec *halfExtentsA, t_vec *halfExtentsB) {
  t_vec *n = &plane->n;
  vec_cross(n, halfExtentsB, halfExtentsA);
  vec_normalize(n);
  plane->uLen = vec_mod(halfExtentsA);
  plane->vLen = vec_mod(halfExtentsB);
  vec_set(&plane->p0, p0);
  setupUV(plane, halfExtentsA, halfExtentsB);
}

__host__ void setupUV(t_plane *plane, t_vec *du, t_vec *dv) {
	vec_normalize(du);
	vec_normalize(dv);
  vec_set(&plane->du, du);
  vec_set(&plane->dv, dv);
}

__device__ 
bool plane_intersection_line(t_plane *plane, t_line *line, t_collision *collision) {
	if (!infinite_plane_intersection(plane, line, collision)) {
		return false;
	}
	t_vec *position = &collision->localPoint;
	double dx = position->x - plane->p0.x;
	double dy = position->y - plane->p0.y;
	double dz = position->z - plane->p0.z;
	if (abs(vec_dot_float(&plane->du, dx, dy, dz)) > plane->uLen) {
		return false;
	}
	if (abs(vec_dot_float(&plane->dv, dx, dy, dz)) > plane->vLen) {
		return false;
	}
	return true;
}

__device__ 
bool infinite_plane_intersection(t_plane *plane, t_line *line, t_collision *collision) {
	float t2 = vec_dot(&line->d, &plane->n);
	if (float_isZero(t2)) {
		return false;
	}
	float t1 = plane_t1(plane, &line->p0);
	float t = t1 / t2;
	if (t < 0) {
		return false;
	}
	collision->distance = t;
	line_pointAt(line, t, &collision->localPoint);
	vec_set(&collision->normal, &plane->n);
	return true;
}

__device__ float plane_t1(t_plane *plane, t_vec *p0) {
	float dx = plane->p0.x - p0->x;
	float dy = plane->p0.y - p0->y;
	float dz = plane->p0.z - p0->z;
	return vec_dot_float(&plane->n, dx, dy, dz);
}

