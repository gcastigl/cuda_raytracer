#ifndef PLANE_H
#define PLANE_H

#include "math3d.h"
#include "../scene/types.h"

__host__
void plane_create(t_plane *plane, t_vec *p0, t_vec *halfExtentsA, t_vec *halfExtentsB);

__device__ 
bool plane_intersection_line(t_plane *plane, t_line *line, t_collision *collision);

#endif

