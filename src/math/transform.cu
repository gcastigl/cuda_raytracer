#include "transform.h"

#define O(y,x) (y + (x<<2))
#define I(row,col) (col + (row<<2))

__device__ void transform_apply(t_matrix4f *matrix, t_vec *v, float k, t_vec *result);
__host__ __device__ t_matrix4f* matrix_id(t_matrix4f *matrix);
__host__ t_matrix4f* matrix_set(t_matrix4f *matrix, const float values[16]);
__host__ bool matrix_inverse(t_matrix4f *matrix, t_matrix4f *inverse);

__host__ __device__ void transform_id(t_transform *transform) {
  matrix_id(&transform->matrix);
  matrix_id(&transform->inverse_matrix);
}

__host__ bool transform_set(t_transform *tansform, const float values[16]) {
  matrix_set(&tansform->matrix, values);
  return transform_update_inverse(tansform);
}

__host__ void transform_translate(t_transform *tansform, float dx, float dy, float dz) {
  tansform->matrix.values[I(0, 3)] = dx;
  tansform->matrix.values[I(1, 3)] = dy;
  tansform->matrix.values[I(2, 3)] = dz;
}

__host__ bool transform_update_inverse(t_transform *tansform) {
  bool hasInverse = matrix_inverse(&tansform->matrix, &tansform->inverse_matrix);
  if (!hasInverse) {
    printf("[ERROR] Matrix is not invertible\n");
    matrix_printf(&tansform->matrix);
  }
  return hasInverse;
}

__device__
void transform_inverse_point(t_transform *transform, t_vec *point, t_vec *result) {
  transform_apply(&transform->inverse_matrix, point, 1, result);
}

__device__
void transform_inverse_direction(t_transform *transform, t_vec *direction, t_vec *result) {
  transform_apply(&transform->inverse_matrix, direction, 0, result);
}

__device__
void transform_direction(t_transform *transform, t_vec *direction, t_vec *result) {
  transform_apply(&transform->matrix, direction, 0, result);
}

__device__
void transform_point(t_transform *transform, t_vec *point, t_vec *result) {
  transform_apply(&transform->matrix, point, 1, result);
}

__device__
void transform_apply(t_matrix4f *matrix, t_vec *v, float k, t_vec *result) {
	float *m = matrix->values;
	float x = m[I(0,0)] * v->x + m[I(0,1)] * v->y + m[I(0,2)] * v->z + m[I(0,3)] * k;
	float y = m[I(1,0)] * v->x + m[I(1,1)] * v->y + m[I(1,2)] * v->z + m[I(1,3)] * k;
	float z = m[I(2,0)] * v->x + m[I(2,1)] * v->y + m[I(2,2)] * v->z + m[I(2,3)] * k;
	vec_setAll(result, x, y, z);
}

__host__ __device__ t_matrix4f* matrix_id(t_matrix4f *matrix) {
  for (int row = 0; row < 4; row++) {
    for (int col = 0; col < 4; col++) {
      matrix->values[I(row, col)] = (row == col) ? 1 : 0;
    }
  }
  return matrix;
}

__host__ t_matrix4f* matrix_set(t_matrix4f *matrix, const float values[16]) {
  memcpy(matrix->values, values, sizeof(float) * 16);
  return matrix;
}

__host__ __device__ t_matrix4f* matrix_mult(t_matrix4f *m1, t_matrix4f *m2, t_matrix4f *result) {
  float *src1 = m1->values;
  float *src2 = m2->values;
  float *dest = result->values;
  *(dest+O(0,0)) = (*(src1+O(0,0)) * *(src2+O(0,0))) + (*(src1+O(0,1)) * *(src2+O(1,0))) + (*(src1+O(0,2)) * *(src2+O(2,0))) + (*(src1+O(0,3)) * *(src2+O(3,0)));
  *(dest+O(0,1)) = (*(src1+O(0,0)) * *(src2+O(0,1))) + (*(src1+O(0,1)) * *(src2+O(1,1))) + (*(src1+O(0,2)) * *(src2+O(2,1))) + (*(src1+O(0,3)) * *(src2+O(3,1)));
  *(dest+O(0,2)) = (*(src1+O(0,0)) * *(src2+O(0,2))) + (*(src1+O(0,1)) * *(src2+O(1,2))) + (*(src1+O(0,2)) * *(src2+O(2,2))) + (*(src1+O(0,3)) * *(src2+O(3,2)));
  *(dest+O(0,3)) = (*(src1+O(0,0)) * *(src2+O(0,3))) + (*(src1+O(0,1)) * *(src2+O(1,3))) + (*(src1+O(0,2)) * *(src2+O(2,3))) + (*(src1+O(0,3)) * *(src2+O(3,3)));
  *(dest+O(1,0)) = (*(src1+O(1,0)) * *(src2+O(0,0))) + (*(src1+O(1,1)) * *(src2+O(1,0))) + (*(src1+O(1,2)) * *(src2+O(2,0))) + (*(src1+O(1,3)) * *(src2+O(3,0)));
  *(dest+O(1,1)) = (*(src1+O(1,0)) * *(src2+O(0,1))) + (*(src1+O(1,1)) * *(src2+O(1,1))) + (*(src1+O(1,2)) * *(src2+O(2,1))) + (*(src1+O(1,3)) * *(src2+O(3,1)));
  *(dest+O(1,2)) = (*(src1+O(1,0)) * *(src2+O(0,2))) + (*(src1+O(1,1)) * *(src2+O(1,2))) + (*(src1+O(1,2)) * *(src2+O(2,2))) + (*(src1+O(1,3)) * *(src2+O(3,2)));
  *(dest+O(1,3)) = (*(src1+O(1,0)) * *(src2+O(0,3))) + (*(src1+O(1,1)) * *(src2+O(1,3))) + (*(src1+O(1,2)) * *(src2+O(2,3))) + (*(src1+O(1,3)) * *(src2+O(3,3)));
  *(dest+O(2,0)) = (*(src1+O(2,0)) * *(src2+O(0,0))) + (*(src1+O(2,1)) * *(src2+O(1,0))) + (*(src1+O(2,2)) * *(src2+O(2,0))) + (*(src1+O(2,3)) * *(src2+O(3,0)));
  *(dest+O(2,1)) = (*(src1+O(2,0)) * *(src2+O(0,1))) + (*(src1+O(2,1)) * *(src2+O(1,1))) + (*(src1+O(2,2)) * *(src2+O(2,1))) + (*(src1+O(2,3)) * *(src2+O(3,1)));
  *(dest+O(2,2)) = (*(src1+O(2,0)) * *(src2+O(0,2))) + (*(src1+O(2,1)) * *(src2+O(1,2))) + (*(src1+O(2,2)) * *(src2+O(2,2))) + (*(src1+O(2,3)) * *(src2+O(3,2)));
  *(dest+O(2,3)) = (*(src1+O(2,0)) * *(src2+O(0,3))) + (*(src1+O(2,1)) * *(src2+O(1,3))) + (*(src1+O(2,2)) * *(src2+O(2,3))) + (*(src1+O(2,3)) * *(src2+O(3,3)));
  *(dest+O(3,0)) = (*(src1+O(3,0)) * *(src2+O(0,0))) + (*(src1+O(3,1)) * *(src2+O(1,0))) + (*(src1+O(3,2)) * *(src2+O(2,0))) + (*(src1+O(3,3)) * *(src2+O(3,0)));
  *(dest+O(3,1)) = (*(src1+O(3,0)) * *(src2+O(0,1))) + (*(src1+O(3,1)) * *(src2+O(1,1))) + (*(src1+O(3,2)) * *(src2+O(2,1))) + (*(src1+O(3,3)) * *(src2+O(3,1)));
  *(dest+O(3,2)) = (*(src1+O(3,0)) * *(src2+O(0,2))) + (*(src1+O(3,1)) * *(src2+O(1,2))) + (*(src1+O(3,2)) * *(src2+O(2,2))) + (*(src1+O(3,3)) * *(src2+O(3,2)));
  *(dest+O(3,3)) = (*(src1+O(3,0)) * *(src2+O(0,3))) + (*(src1+O(3,1)) * *(src2+O(1,3))) + (*(src1+O(3,2)) * *(src2+O(2,3))) + (*(src1+O(3,3)) * *(src2+O(3,3)));
  return result;
}

__host__ bool matrix_inverse(t_matrix4f *matrix, t_matrix4f *inverse) {

  float *m = matrix->values;
  float inv[16];

  inv[0] = m[5]  * m[10] * m[15] - 
           m[5]  * m[11] * m[14] - 
           m[9]  * m[6]  * m[15] + 
           m[9]  * m[7]  * m[14] +
           m[13] * m[6]  * m[11] - 
           m[13] * m[7]  * m[10];

  inv[4] = -m[4]  * m[10] * m[15] + 
            m[4]  * m[11] * m[14] + 
            m[8]  * m[6]  * m[15] - 
            m[8]  * m[7]  * m[14] - 
            m[12] * m[6]  * m[11] + 
            m[12] * m[7]  * m[10];

  inv[8] = m[4]  * m[9] * m[15] - 
           m[4]  * m[11] * m[13] - 
           m[8]  * m[5] * m[15] + 
           m[8]  * m[7] * m[13] + 
           m[12] * m[5] * m[11] - 
           m[12] * m[7] * m[9];

  inv[12] = -m[4]  * m[9] * m[14] + 
             m[4]  * m[10] * m[13] +
             m[8]  * m[5] * m[14] - 
             m[8]  * m[6] * m[13] - 
             m[12] * m[5] * m[10] + 
             m[12] * m[6] * m[9];

  inv[1] = -m[1]  * m[10] * m[15] + 
            m[1]  * m[11] * m[14] + 
            m[9]  * m[2] * m[15] - 
            m[9]  * m[3] * m[14] - 
            m[13] * m[2] * m[11] + 
            m[13] * m[3] * m[10];

  inv[5] = m[0]  * m[10] * m[15] - 
           m[0]  * m[11] * m[14] - 
           m[8]  * m[2] * m[15] + 
           m[8]  * m[3] * m[14] + 
           m[12] * m[2] * m[11] - 
           m[12] * m[3] * m[10];

  inv[9] = -m[0]  * m[9] * m[15] + 
            m[0]  * m[11] * m[13] + 
            m[8]  * m[1] * m[15] - 
            m[8]  * m[3] * m[13] - 
            m[12] * m[1] * m[11] + 
            m[12] * m[3] * m[9];

  inv[13] = m[0]  * m[9] * m[14] - 
            m[0]  * m[10] * m[13] - 
            m[8]  * m[1] * m[14] + 
            m[8]  * m[2] * m[13] + 
            m[12] * m[1] * m[10] - 
            m[12] * m[2] * m[9];

  inv[2] = m[1]  * m[6] * m[15] - 
           m[1]  * m[7] * m[14] - 
           m[5]  * m[2] * m[15] + 
           m[5]  * m[3] * m[14] + 
           m[13] * m[2] * m[7] - 
           m[13] * m[3] * m[6];

  inv[6] = -m[0]  * m[6] * m[15] + 
            m[0]  * m[7] * m[14] + 
            m[4]  * m[2] * m[15] - 
            m[4]  * m[3] * m[14] - 
            m[12] * m[2] * m[7] + 
            m[12] * m[3] * m[6];

  inv[10] = m[0]  * m[5] * m[15] - 
            m[0]  * m[7] * m[13] - 
            m[4]  * m[1] * m[15] + 
            m[4]  * m[3] * m[13] + 
            m[12] * m[1] * m[7] - 
            m[12] * m[3] * m[5];

  inv[14] = -m[0]  * m[5] * m[14] + 
             m[0]  * m[6] * m[13] + 
             m[4]  * m[1] * m[14] - 
             m[4]  * m[2] * m[13] - 
             m[12] * m[1] * m[6] + 
             m[12] * m[2] * m[5];

  inv[3] = -m[1] * m[6] * m[11] + 
            m[1] * m[7] * m[10] + 
            m[5] * m[2] * m[11] - 
            m[5] * m[3] * m[10] - 
            m[9] * m[2] * m[7] + 
            m[9] * m[3] * m[6];

  inv[7] = m[0] * m[6] * m[11] - 
           m[0] * m[7] * m[10] - 
           m[4] * m[2] * m[11] + 
           m[4] * m[3] * m[10] + 
           m[8] * m[2] * m[7] - 
           m[8] * m[3] * m[6];

  inv[11] = -m[0] * m[5] * m[11] + 
             m[0] * m[7] * m[9] + 
             m[4] * m[1] * m[11] - 
             m[4] * m[3] * m[9] - 
             m[8] * m[1] * m[7] + 
             m[8] * m[3] * m[5];

  inv[15] = m[0] * m[5] * m[10] - 
            m[0] * m[6] * m[9] - 
            m[4] * m[1] * m[10] + 
            m[4] * m[2] * m[9] + 
            m[8] * m[1] * m[6] - 
            m[8] * m[2] * m[5];     
  float det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
  if (det == 0) {
    return false;
  }
  det = 1.0 / det;
  for (int i = 0; i < 16; i++) {
    inverse->values[i] = inv[i] * det;
  }
  return true;
}

__device__ __host__ void matrix_printf(t_matrix4f *matrix) {
  float *vs = matrix->values;
  printf(
    " %.4f %.4f %.4f %.4f\n %.4f %.4f %.4f %.4f\n %.4f %.4f %.4f %.4f\n %.4f %.4f %.4f %.4f\n\n", 
    vs[I(0,0)], vs[I(0,1)], vs[I(0,2)], vs[I(0,3)],
    vs[I(1,0)], vs[I(1,1)], vs[I(1,2)], vs[I(1,3)],
    vs[I(2,0)], vs[I(2,1)], vs[I(2,2)], vs[I(2,3)],
    vs[I(3,0)], vs[I(3,1)], vs[I(3,2)], vs[I(3,3)]
  );
}

