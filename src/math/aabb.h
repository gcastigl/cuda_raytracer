#ifndef MATH_AABB_H
#define MATH_AABB_H

#include "math3d.h"
#include "../scene/types.h"

__host__ void aabb_create(t_aabb *aabb, t_vec *min, t_vec *max);

__device__ bool aabb_intersects(t_aabb *aabb, t_line *line);

#endif

