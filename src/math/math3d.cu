#include <stdio.h>
#include <iostream>

#include "math3d.h"

using namespace std;

__device__ void swap(float& a, float& b) {
    float c = a; a=b; b=c;
}

__device__ float float_max(float n1, float n2) {
  return n1 > n2 ? n1 : n2;
}

__device__ float float_sq(float f) {
  return f * f;
}

__host__ __device__ float float_eq(float n1, float n2) {
  return abs(n1 - n2) < 0.0001;
}

__host__ __device__ float float_isZero(float n1) {
  return float_eq(n1, 0);
}

__device__ float float_lenghtSq(float x, float y, float z) {
	return x * x + y * y + z * z;
}

__host__ __device__ float* floats_printf(float *ns, int count) {
  for (int i = 0; i < count; i++) {
    printf("%.4f ", ns[i]);
  }
  printf("\n");
  return ns;
}

__host__ __device__ int* ints_printf(int *ns, int count) {
  for (int i = 0; i < count; i++) {
    printf("%d ", ns[i]);
  }
  printf("\n");
  return ns;
}

__device__ bool vec_equals(t_vec *p1, t_vec *p2) {
  return float_eq(p1->x, p2->x) && float_eq(p1->y, p2->y) && float_eq(p1->z, p2->z);
}

__host__ __device__ t_vec* vec_printf(t_vec *p) {
  printf("(%.3f, %.3f, %.3f)\n", p->x, p->y, p->z);
  return p;
}

__host__ __device__ void vec_printf_2(t_vec *p1, t_vec *p2) {
  printf("(%.3f, %.3f, %.3f) => (%.3f, %.3f, %.3f)\n", 
    p1->x, p1->y, p1->z, p2->x, p2->y, p2->z
  );
}

__host__ __device__ t_vec* vec_set(t_vec *target, t_vec *source) {
  target->x = source->x;
  target->y = source->y;
  target->z = source->z;
  return target;
}

__device__ t_vec* vec_scale(t_vec *target, float scale) {
  target->x *= scale;
  target->y *= scale;
  target->z *= scale;
  return target;
}

__device__ t_vec* vec_add(t_vec *target, t_vec *add) {
  target->x += add->x;
  target->y += add->y;
  target->z += add->z;
  return target;
}

__device__ t_vec* vec_sub(t_vec *target, t_vec *substract) {
  target->x -= substract->x;
  target->y -= substract->y;
  target->z -= substract->z;
  return target;
}

__host__ __device__ t_vec* vec_setAll(t_vec *target, float x, float y, float z) {
  target->x = x;
  target->y = y;
  target->z = z;
  return target;
}

__host__ __device__ float vec_dot(t_vec *p1, t_vec *p2) {
  return p1->x * p2->x + p1->y * p2->y + p1->z * p2->z;
}

__device__ float vec_dot_float(t_vec *p1, float x, float y, float z) {
  return p1->x * x + p1->y * y + p1->z * z;
}

__host__ __device__ float vec_modSq(t_vec *p) {
  return vec_dot(p, p);
}

__host__ __device__ float vec_mod(t_vec *p) {
  return sqrt(vec_modSq(p));
}

__host__ __device__ float vec_normalize(t_vec *vec) {
  float mod = vec_mod(vec);
  if (!float_isZero(mod)) {
    vec->x /= mod;
    vec->y /= mod;
    vec->z /= mod;
  } else {
    printf("Null module for vector: (%.3f, %.3f, %.3f)\n", vec->x, vec->y, vec->z);
  }
  return mod;
}

__device__ t_vec* vec_sub(t_vec *p1, t_vec *p2, t_vec *target) {
  target->x = p1->x - p2->x;
  target->y = p1->y - p2->y;
  target->z = p1->z - p2->z;
  return target;
}

__device__ t_vec* vec_mult(t_vec *target, t_vec *mult) {
  target->x *= mult->x;
  target->y *= mult->y;
  target->z *= mult->z;
  return target;
}

__device__ float vec_distanceSq(t_vec *v1, t_vec *v2) {
  float dx = v1->x - v2->x;
  float dy = v1->y - v2->y;
  float dz = v1->z - v2->z;
  return dx * dx + dy * dy + dz * dz;
}

__host__ __device__ t_vec* vec_addScaled(t_vec *add, float scale, t_vec *target) {
  target->x += add->x * scale;
  target->y += add->y * scale;
  target->z += add->z * scale;
  return target;
}


__device__ float vec_crossLen(float x1, float y1, float z1, float x2, float y2, float z2) {
	float x, y, z;
	x = y1 * z2 - z1 * y2;
	y = x2 * z1 - z2 * x1;
	z = x1 * y2 - y1 * x2;
	return sqrt(float_lenghtSq(x, y, z));
}

__host__ __device__ t_vec* vec_cross(t_vec *cross, t_vec *a, t_vec *b) {
  float x = a->y * b->z - a->z * b->y;
  float y = a->z * b->x - a->x * b->z;
  float z = a->x * b->y - a->y * b->x;
  cross->x = x;
  cross->y = y;
  cross->z = z;
  return cross;
}

