#ifndef TRAVERSER_H
#define TRAVERSER_H

#include "../scene/types.h"

typedef struct {
  int depth;
  int thread_local_index;
  int thread_global_index;
  t_render_mode renderMode;
} t_ray_info;

__device__
void raytracer_trace(
  t_line *line, t_scene *scene, t_collision *collision, t_ray_info *info
);

#endif

