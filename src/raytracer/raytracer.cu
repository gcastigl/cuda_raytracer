#include <stdio.h>
#include <iostream>

#include "raytracer.h"
#include "../light/light_shader.h"
#include "../scene/scene_traverser.h"

__device__ void raytracer_solveReflectance(
  t_scene *scene, t_line *line, t_collision *collision
);

__device__ void raytracer_directLighting(
  t_scene *scene, t_line *line, t_collision *collision
);


__device__
void raytracer_trace(t_line *line, t_scene *scene, t_collision *collision, t_ray_info *info) {
  vec_setAll(&collision->color, 0, 0, 0);
  collision->geometry = NULL;
  bool hasCollision = scene_traverser_closest(scene, line, collision);
  if (hasCollision && info->renderMode == MODE_COLOR) {
    // printf("[DEBUG] Thread %d has collision\n", info->thread_global_index);
    raytracer_solveReflectance(scene, line, collision);
  }
}

__device__
void raytracer_solveReflectance(t_scene *scene, t_line *line, t_collision *collision) {
  t_geometry *geometry = collision->geometry;
  t_meterial_type type = geometry->material.type;
  if (type == MATTE) {
    raytracer_directLighting(scene, line, collision);
  } else {
    printf("Unsupported material type: %d for geometry\n", type);
  }
}

__device__
void raytracer_directLighting(t_scene *scene, t_line *line, t_collision *collision) {
	// FIXME: the following line should not be necessary but at some point it gets filled with invlid values!
	vec_setAll(&collision->color, 0, 0, 0);
	if (scene->lightsCount == 0) {
	  return;
	}
	t_vec color;
	t_vec eye;
	vec_set(&eye, &line->d);
	vec_scale(&eye, -1);
	t_light *lights = scene->lights;
	for (int i = 0; i < scene->lightsCount; i++) {
	  vec_setAll(&color, 0, 0, 0);
	  light_shader_apply(lights + i, scene, collision, &eye, &color);
	  vec_add(&collision->color, &color);
	}
}

