#!/bin/bash -e
echo "> Compiling"
nvcc src/*cu src/math/*cu src/light/*cu src/scene/*cu src/shader/*cu \
  src/parser/*cu src/raytracer/*cu src/image/*cu src/material/*cu \
  src/mesh/*cu -I ./libs/pugixml-1.8/ \
  -lm -lpng -dc --output-directory build
nvcc build/* ./libs/pugixml-1.8/pugi.o -lpng -o raytracer.out
echo ">> Compiled OK"

